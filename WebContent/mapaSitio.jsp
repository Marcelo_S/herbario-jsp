<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="./resources/css/app.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>
<body>
    <header class="Header">
            <div class="Header-menu">
                <div class="container Header-containerMostWidht">
                    <div class="Header-wrapper">
                        <!--LOGO-->
                        <!--MENU logo-->
                        <img src="public/img/menuLogo.png" alt="logo" class="Header-menuLogo">
                        <!--MENU HAMBURGUESA-->
                        <label for="menuCheck" class="Header-Hamburguer mb-0">
                            <i class="fas fa-bars"></i>
                        </label>
                        <input type="checkbox" name="menuCheck" id="menuCheck" class="Header-HamburguerCheck d-none" autocomplete="off">
                        <!--MENU DEL SITIO-->
                        <nav class="Header-navBar">
                            <a href="index.jsp" class="Header-menuItem">Inicio</a>
                            <a href="News&events.jsp" class="Header-menuItem">Anuncios</a>
                            <a href="AboutUs.jsp" class="Header-menuItem">Servicios</a>
                            <a href="subscribe.jsp" class="Header-menuItem">Suscripción</a>
                            <a href="tips.jsp" class="Header-menuItem">Muestra</a>
                            <a href="contact.jsp" class="Header-menuItem">Contactos</a>
                            <a href="mapaSitio.jsp" class="Header-menuItem page">Mapa del sitio</a>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="Header-banner contactSection">
                <div class="container Header-containerMostWidht contentCenter">
                    <img src="public/img/logo.png" alt="" class="Header-logo">
                </div>
            </div>
    </header>
    <section>
        <div class="container">
            <h2 class="AboutUs-title">MAPA DEL SITIO</h2>
            <img class="img-fluid" src="public/img/Mapa del sitio.png" alt="mapa de sitio">
        </div>
    </section>
</body>
</html>